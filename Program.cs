﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Net;

namespace Module2
{
    public class Program
    {
        static void Main(string[] args)
        {
            
            Program a = new Program();            
            Dimensions b= new Dimensions();
            
            b.FirstSide = 10;
            b.SecondSide = 20;
            b.ThirdSide = 25;
            b.Height = 0;
            b.Diameter = 0;
            b.Radius = 0;
            
            Console.WriteLine(a.GetTotalTax(8848, 666, 99));
            Console.WriteLine(a.GetCongratulation(19));
            Console.WriteLine(a.GetMultipliedNumbers("123.456", "1000.0"));
            Console.WriteLine(a.GetFigureValues(FigureEnum.Triangle, ParameterEnum.Perimeter, b));
            
        }
        public int GetTotalTax(int companiesNumber, int tax, int companyRevenue)
        {
            return Convert.ToInt32(companiesNumber * companyRevenue * tax / 100.0);
        }        
        
        public string GetCongratulation(int input)
        {
            if (input >= 18 && input % 2 == 0)
                return "Поздравляю с совершеннолетием!";
            else if (input > 12 && input < 18 && input % 2 > 0)
                return "Поздравляю с переходом в старшую школу!";
            else return $"Поздравляю с {input}-летием!";
        }

        public double GetMultipliedNumbers(string first, string second)
        {
            try
            {                
                double f = Convert.ToDouble(first.Replace(',', '.'),CultureInfo.InvariantCulture);
                double s = Convert.ToDouble(second.Replace(',', '.'), CultureInfo.InvariantCulture);

                return f * s;
            }
            catch
            {
                throw new ArgumentException("не число");
            }            

        }

        public double GetFigureValues(FigureEnum figureType, ParameterEnum parameterToCompute, Dimensions dimensions)
        {
            if(FigureEnum.Triangle == figureType)
            {
                if (ParameterEnum.Square == parameterToCompute)
                {
                    if (dimensions.Height != 0)
                    {
                        return (int)(dimensions.FirstSide * dimensions.Height) / 2.0;
                    }
                    else
                    {
                        double p = (dimensions.FirstSide + dimensions.SecondSide + dimensions.ThirdSide) / 2.0;
                        return (int)(Math.Sqrt(p * (p - dimensions.FirstSide) * (p - dimensions.SecondSide) * (p - dimensions.ThirdSide)));
                    }
                }
                else return dimensions.FirstSide + dimensions.SecondSide + dimensions.ThirdSide;
            }
            else if (FigureEnum.Rectangle == figureType)
            {
                if (ParameterEnum.Square == parameterToCompute) return (int)(dimensions.FirstSide * dimensions.SecondSide);
                else return (int)((dimensions.FirstSide + dimensions.SecondSide) * 2);
            }
            else  
            {
                if (ParameterEnum.Square == parameterToCompute) return Convert.ToInt32((Math.PI * dimensions.Radius * dimensions.Radius));
                else return Convert.ToInt32((2 * Math.PI * dimensions.Radius));
            }         

        }        
    }
}
